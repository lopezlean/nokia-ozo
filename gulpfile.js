/**
* Gulp file for OZO template
* @author Ligrila Software <info@ligrila.com>
*
*/


var gulp        = require('gulp');
var browserSync = require('browser-sync');
var sass        = require('gulp-sass');
var sourcemaps  = require('gulp-sourcemaps');
var prefix      = require('gulp-autoprefixer');
var cp          = require('child_process');

var jekyll   = process.platform === 'win32' ? 'jekyll.bat' : 'jekyll';
var messages = {
    jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'
};

/**
 * Build the Jekyll Site
 */
gulp.task('jekyll-build', function (done) {
    browserSync.notify(messages.jekyllBuild);
    return cp.spawn( jekyll , ['build'], {stdio: 'inherit'})
        .on('close', done);
});

/**
 * Rebuild Jekyll & do page reload
 */
gulp.task('jekyll-rebuild', ['jekyll-build'], function () {
    browserSync.reload();
});

/**
 * Wait for jekyll-build, then launch the Server
 */
gulp.task('browser-sync', ['sass', 'jekyll-build'], function() {
    browserSync({
        server: {
            baseDir: 'site'
        }
    });
});

/**
 * Compile sass
 */
gulp.task('sass', function () {
    console.log('Compiling sass');
    return gulp.src('scss/main.scss')
      .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['scss'],
            onError: browserSync.notify
        }).on('error', sass.logError))
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('site/vr/assets/css'))
        .pipe(browserSync.reload({stream:true}))
        .pipe(gulp.dest('site/vr/assets/css'));
});

// gulp.task('assets',function(){
//   gulp.src(['fonts/**/*']).pipe(gulp.dest('site/fonts'));
//   gulp.src(['js/**/*']).pipe(gulp.dest('site/js'));
//   gulp.src(['img/**/*']).pipe(gulp.dest('site/img'));
// });

/**
 * Watch scss files for changes & recompile
 * Watch html/md files, run jekyll & reload BrowserSync
 */
gulp.task('watch', function () {
    gulp.watch('scss/**', ['sass']);
    gulp.watch([ 'html/**/*.html'], ['jekyll-rebuild']);
    gulp.watch([ '*.html'], ['jekyll-rebuild']);
//     gulp.watch([ 'fonts/**/*'], ['assets']);
//     gulp.watch([ 'js/**/*'], ['assets']);
});

/**
 * Default task, running just `gulp` will compile the sass,
 * compile the jekyll site, launch BrowserSync & watch files.
 */
gulp.task('default', ['browser-sync', 'watch']);
