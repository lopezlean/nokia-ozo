# Getting Started

*If you've previously installed gulp globally, run `npm rm --global gulp` before following these instructions.*

#### Install the `gulp` command

```sh
npm install --global gulp-cli
```

#### Run `gulp`

Run this command in your project directory:

```sh
gulp
```