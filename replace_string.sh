#!/bin/bash
for i in $(find . -type d)
do
	if [ -d $i ]; then
		sed -i "s/$1/$2/g" $i/*.*
	fi
   
done
