---
layout: default
title: OZO Player SDK
permalink: /using-the-sdk/
bodyClass: page-specs
meta-title: Get 360 VR Video Player SDK to publish & distribute VR to platforms | Nokia OZO
meta-description: OZO Player SDK is a VR playback software development kit allowing developers to quickly and efficiently build VR video applications for every major viewing platform
---
<div class="intro intro-gray">
  <div class="container">
    <div class="intro-title">
      <h2>Using OZO Player SDK</h2>
    </div>
  </div>
</div>





<div class="intro m-t-5">
  <div class="container bg-gray">
    <div class="intro-title font-weight-normal">
      <p><strong>TABLE OF CONTENTS</strong></p>
      <ol>
      <li><a href="#item-1">Using the SDK</a></li>
      <li><a href="#item-2">Creating a custom HMD device implementation</a></li>
      </ol>
    </div>
  </div>
</div>

{% capture content %}

<p>The main steps in using the SDK are:</p>
<ol>
<li><a href="#item-1-1">Creating the SDK instance</a></li>
<li><a href="#item-1-2">Setting up the audio playback</a></li>
<li><a href="#item-1-3">Setting up the rendering</a></li>
<li><a href="#item-1-4">Starting and controlling the playback</a></li>
<li><a href="#item-1-5">Rendering the video and audio with the correct orientation</a></li>
<li><a href="#item-1-6">Foreground / background handling</a></li>
<li><a href="#item-1-7">SDK shutdown and releasing of resources</a></li>
</ol>

<p>
More detailed API descriptions are in the API reference document. If you're integrating the SDK to an existing app, then the best examples are the simple sample apps located in the Samples folder since those contain only the very basic things you need to do in order to use the SDK. If you want a more complete example of an application using the SDK, then you should take a look at the OZO Player App. Page Break</p>

<p class="font-weight-normal" id="item-1-1">Creating an SDK instance</p>

<p>An SDK instance is created by calling the create method of the IOZOPlayer class.</p>

<pre>
OZO::PlatformParameters platformParameters;
// Fill the plaftorm parameters with platform specific information
OZO::AnalyticsParameters analyticsParameters;
analyticsParameters.licenseID = "ENTER_YOUR_LICENCE_ID_HERE"
OZO::IOZOPlayer* ozoPlayer = OZO::IOZOPlayer::create(graphicsAPI, &platformParameters, &analyticsParameters);
</pre>


<p>To create the instance, you need to specify which graphics API should be used by the SDK. Currently OpenGL, OpenGL ES and DX11 are supported. You also need to provide a struct with platform specific initialization information, such as default storage location of videos. Further, you need to specify parameters for analytics - for details see next section.<br/>
Creating the SDK instance also creates the interfaces for controlling audio (IAudio), rendering (IRenderer) and playblack (IPlaybackControls).</p>

<p>Analytics</p>

<p>When creating SDK instance, you also need to specify parameters for analytics (AnalyticsParameters). The struct contains two parameters: a flag to enable/disable analytics and licenseID string. The licenseID string is mandatory.</p>

<p>If you're using the Free version of the SDK, the licenseID should be the package ID of the application (in the form: com.companyname.applicationname). It should be the same package ID / bundle ID than the application has for Android or iOS, respectively. For Windows the same pattern should be followed.</p>

<p>If you are using the paid version of the SDK, then use the licenseID provided by Nokia.</p>

<p>Analytics are enabled by default. It depends on the license type if the flag to disable it is functional and hence if the application can disable analytics. Analytics is used to improve user experience of OZO Player SDK. It requires that your application is able to access Internet, and hence may require to specify platform specific permission / capability.</p>

<p>Nokia does not collect any personally identifiable data, and all privacy aspects described in the license agreement apply.</p>

<p class="font-weight-normal" id="item-1-2">Setting up the audio:</p>

<p>The SDK provides two options for audio playback: The first option is direct routing where the SDK itself handles audio playback all the way from the file to the speakers. With direct routing you can allow exclusive mode which will allow the application to run the audio playback at a better latency. However it will mean that other apps can't access the audio hardware.</p>

<pre>
bool_t allowExclusiveModeInAudio = false;
OZO::Result::Enum result = ozoPlayer->getAudio()->initializeAudioWithDirectRouting(allowExclusiveModeInAudio);
</pre>
<p>
The second option is custom routing where the user of the SDK implements the last step of the audio playback. In this case the user must implement the AudioObserver interface to receive the PCM audio buffers from the SDK. This requires you to also implement other related functionality, e.g. responding to elapsed time queries to maintain AV-sync.</p>


<p>Our recommendation is to use direct routing unless you need for one reason or another access the audio output yourself. If you need to implement a custom audio routing solution, please contact our support for further instruction.</p>

<p  class="font-weight-normal" id="item-1-3">Setting up video rendering:</p>

<p>For rendering, you can either use the HMD device implementations provided by the SDK or create your own. If you use the implementations provided by the SDK, you just need to create an instance of the HMD you wish to use and initialize it. The HMD implementation will create all the render targets internally. If you don't use the SDK's implementations, you need to setup the eye surfaces for the renderer. The eye surface contains the render target with the render target handle and size information, the eye position which is used to decide whether left or right eye content is rendered, and the projection for the view. The render target handle should be created by calling the
createNativeRenderTarget method of IOZOPlayer.</p>

<pre>
uint32_t viewPortWidth; // Fill this with your viewport width
uint32_t viewPortHeight; // Fill this with your viewport height
GLuint color; // Set this to your render target GL texture
RenderTextureDesc colorTextureDesc;
colorTextureDesc.type = TextureType::TEXTURE_2D;
colorTextureDesc.format = RenderTextureFormat::R8G8B8A8;
colorTextureDesc.width = width;
colorTextureDesc.height = height;
colorTextureDesc.ptr = &color;

OZO::RenderSurface surface;
OZO::Matrix44 projectionMatrix; // Fill this with your projection
OZO::Matrix44 eyeTranslation; // Fill this with your eye translation

surface->handle = ozoPlayer()->getRenderer()->createRenderTarget(&colorTextureDesc, NULL);
surface->viewport.x = 0;
surface->viewport.y = 0;
surface->viewport.width = width;
surface->viewport.height = height;
surface->eyePosition = OZO::EyePosition::LEFT;
surface->projection = projectionMatrix;
surface->eyeTransform = eyeTranslation
</pre>

<p>We have provided HMD implementations for the following devices:</p>

<p>HTC Vive and Oculus Rift on Windows<br/>
GearVR and DayDream / Google VR on Android<br/>
Google VR on iOS</p>

<p class="font-weight-normal" id="item-1-4">Starting and controlling playback:</p>

<p>The interface IPlaybackControls contains the controls for loading a new video, either a local file or a compatible DASH stream, controlling the playback and also getting duration information about the loaded video. The state of the video playback can be fetched using the getPlaybackState method</p>

<pre>
OZO::IPlaybackControls *playbackControls = ozoPlayer->getPlaybackControls();
OZO::VideoPlaybackState::Enum state = playbackControl->getVideoPlaybackState();
</pre>
<p>
Calling loadVideo loads the video file but doesn’t start playback.<br/>
State changes for loadVideo: IDLE -> (sync) LOADING -> (async) PAUSED/STREAM_ERROR/CONNECTION_ERROR
</p>
<pre>
playbackControl->loadVideo("storage://some_video.mp4");
</pre>
<p>
Calling play starts/resumes the video playback and the playback can be paused or seeked with pause and seek. Play and pause are synchronous. Calling play directly after loadVideo will block the calling thread until the video is loaded.<br/>
State changes for play: LOADING/PAUSED -> (sync) BUFFERING/PLAYING<br/>
State changes for pause: PLAYING/BUFFERING -> (sync) PAUSED<br/>
</p>
<pre>
playbackControl->play();
playbackControl->pause();
</pre>
<p>
Calling stop will stop the playback and release resources associated with the loaded video. Hence a new LoadVideo call is needed to play the same file again.<br/>
State changes for stop: ANY STATE -> (sync) IDLE
</p>
<pre>
playbackControl->stop();
</pre>

<p  class="font-weight-normal" id="item-1-5">Rendering the video and audio:</p>

<p>During playback the video can be rendered to the eye surfaces using the methods in the IRenderer interface. At this point you should call the IAudio method for updating the head transformation to ensure that video and audio are directionally in sync.<p>

<pre>
OZO::IHMDDevice* hmd;
OZO::HeadTransform headTransform = hmd->getHeadTransform();
ozoPlayer->getAudio()->setHeadTransform(headTransform);
</pre>
<p>
To render the video frames, you should call renderSurfaces with the current HeadTransform and all the RenderSurfaces you want to update. To ensure that all the surfaces are timesynched, call this only once per rendering cycle so that all the surfaces are listed in a single call.
</p>
<pre>
uint8_t eyeCount = hmd->getEyeCount();
uint8_t surfaceCount = hmd->getRenderSurfaceCount();
uint8_t numRenderSurfaces = eyeCount * surfaceCount;
ozoPlayer->getRenderer()->renderSurfaces(headTransform, hmd->getRenderSurfaces(), numRenderSurfaces);
</pre>

<p>The output of the renderer is an equirectangular image which should be used as an input for the HMD reverse distortion rendering.<br/>
When rendering without an HMD, you can render the renderer output directly on screen and it should look correct. The SDK provides a default renderer HMDDevice implementation for each platform which can be used to draw directly on screen without an HMD. If you’re using the HMD implementations provided by the SDK, call the submit method of the HMD to render the eye buffers on the device.
</p>

<p>You can also provide additional RenderingParameters to adjust the rendering result. With the rendering parameters you can enable the watermark in the Pro version (it's always on in the Free version), force monoscopic rendering with which the right eye content is drawn for both eyes. You can also define clipping areas which will be rendered with the provided clear color instead of video data. The clear color is used also for the back parts of a 180 degree video.</p>

<p class="font-weight-normal" id="item-1-6">Foreground / background handling</p>

<p>The SDK provides an API for foreground and background handling. You can call suspend when the application goes to the background and resume when it returns. During suspend the SDK will save the current location of the file being played back and then release the resources used by the SDK. Calling resume will then cause the SDK to load the resources again, reload the file and if possible seek to the location saved in the suspend call.
The suspend/resume API should be used at least with mobile platforms since it releases the graphics, video decoder and other hardware resources used by SDK which should not be held by apps which aren't in the foreground.</p>

<pre>
ozoPlayer->suspend();

ozoPlayer->resume();
</pre>

<p class="font-weight-normal" id="item-1-7">SDK shutdown:</p>


<p>To shutdown the SDK, destroy the IOZOPlayer instance by calling the destroy method of IOZOPlayer. Do not try to destroy any of the SDK interfaces by calling delete directly as this will most likely result in a crash. The HMD device implementations can be deleted normally.</p>

<pre>
OZO::IOZOPlayer::destroy(ozoPlayer);
</pre>

{%endcapture%}
{%
  include tutorial/item.html
  id= "item-1"
  title= "1. Using the SDK:"
  content= content
%}

{%capture content%}

<p>You may create your own custom HMD device implementation by implementing the IHMDDevice interface.</p>

<p>In order to ensure consistency with the HMD devices provided by the SDK, you should implement your custom HMD in the following way:</p>
<ul>
<li>Constructor of the HMD device detects whether the HMD runtime is installed on computer and if the device is 		   plugged in or not.</li>
<li>The initialize method should reserve all the resources required by the HMD and set the HMD device to a state 			   where it can start to render frames.</li>
<li>The update method should update all state information of the HMD, including the head tracking information. If 		   the HMD has not been initialized yet, the update method should recheck if the HMD is plugged in.</li>
<li>The getHeadtransform method should return the head transform cached during the last update call.</li>
<li>The recenterPose method should reset the yaw of the head tracking, either by calling the HMD framework to do 		   it or by manually applying an offset.</li>
<li>SubmitFrame method should render the content currently in the eye surface buffers on the device.</li>
</ul>
<p>For more detailed examples, please take a look at any of the HMD device implementation that come with the SDK.</p>
{%endcapture%}
{%
  include tutorial/item.html
  id= "item-2"
  title= "2. Creating a custom HMD device implementation"
  content= content
%}


{% include commons/newsletter.html %}
