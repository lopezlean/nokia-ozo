<?php

$response = array(
  'saved'=>false,
  'error'=>'Please insert an email address'

);

$error_code = 0;

if(!empty($_GET['errorcode'])){
  $error_code = (int)$_GET['errorcode'];
  switch ($error_code) {
    case 1:
    case 6:
    case 7:
    case 9:
    case 10:
      $response['error'] = 'An error has occurred while attempting to save your subscriber information.';
      break;
    case 2:
        $response['error'] = 'The list provided does not exist.';
        break;
    case 3:
      $response['error'] = 'Information was not provided for a mandatory field.';
      break;
    case 4:
      $response['error'] = 'Invalid information was provided.	';
      break;
    case 5:
    $response['error'] = 'Information provided is not unique.	';
    break;
    case 8:
    $response['error'] = 'Subscriber already exists.';
    break;
    case 12:
    $response['error'] = 'The subscriber you are attempting to insert is on the master unsubscribe list or the global unsubscribe list.	';
    break;
    case 13:
    $response['error'] = 'The List ID or MID isn\'t correct';
    break;
    case 14:
    $response['error'] = 'The query string contains ETsubscriberKey and the Subscriber Key feature is not enabled on your account.	';
    break;

    default:
      # code...
      break;
  }
}

echo json_encode($response);
