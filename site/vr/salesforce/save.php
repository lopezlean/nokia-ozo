<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/vendor/autoload.php';

$fields = http_build_query($_POST);
$ch = curl_init();
$testServer = "https://test.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8";
$prodServer = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8";

$server = $prodServer;
if(isset($_GET['testing'])){
  $server = $testServer;
}

if (!isset($_POST['g-recaptcha-response'])){
  echo json_encode(array('saved'=>false,'error'=>'Error submitting newsletter. Missing recaptcha. Please, try again.'));
  exit;

}


// Register API keys at https://www.google.com/recaptcha/admin
$siteKey = '6LfAbioUAAAAANt-TYCPMofCOAFn5wrBuMcTPu_3';
$secret = '6LfAbioUAAAAAJG5bvQtQTAlONadrU9vwB4ElKl5';

$recaptcha = new \ReCaptcha\ReCaptcha($secret);
$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

if (!$resp->isSuccess()){
  echo json_encode(array('saved'=>false,'error'=>'Error submitting newsletter. Invalid recaptcha. Please, try again.'));
  exit;

}




curl_setopt($ch, CURLOPT_URL,$server);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);
// further processing ....
if (!empty($server_output)) {
  echo json_encode(array('saved'=>true));
} else {
  echo json_encode(array('saved'=>false,'error'=>'Error submitting newsletter. Please, try again.'));
 }

?>
