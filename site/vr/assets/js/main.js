var Ozo = {};
Ozo.isMobile = false;


Ozo.initHorizontalCheckbox = function(){
  var requiredCheckboxes = $('.horizontal-checkbox :checkbox[required]');

 requiredCheckboxes.change(function(){

     if(requiredCheckboxes.is(':checked')) {
         requiredCheckboxes.removeAttr('required');
     }

     else {
         requiredCheckboxes.attr('required', 'required');
     }
 });
}

Ozo.initSubscribeForm = function(){
  $("#subscribe-form").submit(function () {
        var email = $("#subscribe-form input").val();
        var action = $("#subscribe-form").attr("action");
        $.getJSON(action + "?email=" + email, function (data) {
            if (data.response.saved) {
                $("#newsletter-submit-button").hide();
                $("#newsletter-success-button").removeClass("hidden");
                $("#subscribe-form input[type!='hidden']").val("");
            } else {

                alert("Please insert an email address");
            }
        });

        return false;
      });

          $("#connect-subscribe-form").submit(function () {
              var email = $("#connect-subscribe-form input").val();
              var action = $("#connect-subscribe-form").attr("action");
              $.getJSON(action + "?email=" + email, function (data) {
                  if (data.response.saved) {
                      $("#connect-newsletter-submit-button").hide();
                      $("#connect-newsletter-success-button").removeClass("hidden");
                      $("#connect-subscribe-form input[type!='hidden']").val("");
                  } else {
                      alert("Please insert an email address");
                  }
              })
              return false;

        });
}

Ozo.initCannesContactForm = function(){

  var options = {
      dataType: "jsonp",
      error: onError,
      iframe: true,
      beforeSubmit: beforeSubmit,
      success:       onFormResponse  // post-submit callback
  };

  $('#cannes-subscribe-form').ajaxForm(options);
  var domain = document.URL.split("/")[2];
  var protocol = window.location.protocol;

  $("#cannes-subscribe-form input[name='retURL']").val(protocol+"//"+domain+Request.base+"/newsletter_thanks.html");

  var onFormResponse = function(response){
    response = JSON.parse(response);
    if (response.saved) {
      $("#cannes-subscribe-form-submit-button").hide();
      $("#cannes-subscribe-form-success-button").removeClass("hidden");
      $("#cannes-subscribe-form input[type!='hidden']").val("");
      $("#cannes-subscribe-form textarea").val("");
    } else {
      alert("Error submitting newsletter. Please, try again.");
    }
  };

  var beforeSubmit = function(){
    //$("#connect-subscribe-form input").attr("readonly", "readonly");
    return true;
  };

  var onError = function(err){
    alert("Error submitting newsletter. Please, try again.");

  }

  var options = {
      dataType: "jsonp",
      error: onError,
      iframe: true,
      beforeSubmit: beforeSubmit,
      success:       onFormResponse  // post-submit callback
  };

  $('#cannes-subscribe-form').ajaxForm(options);
  $("#cannes-subscribe-form input[name='Page']").val(window.location.href);

}
Ozo.initSubscribeForm2 = function () {
  var onFormResponse = function(response){
    response = JSON.parse(response);
    if (response.saved) {
        $("#newsletter-submit-button").hide();
        $("#newsletter-success-button").removeClass("hidden");
        $("#subscribe-form input").val("");
    } else {
        alert(response.error);
    }
  };

  var beforeSubmit = function(){
    //$("#subscribe-form input").attr("readonly", "readonly");
    return true;
  };

  var onError = function(err){
    //$("#subscribe-form input").removeAttr("readonly");
    alert("Error submitting newsletter. Please, try again.");

  }

  var options = {
      dataType: "jsonp",
      error: onError,
      iframe: true,
      beforeSubmit: beforeSubmit,
      success:       onFormResponse  // post-submit callback
  };

  // bind form using 'ajaxForm'
    var domain = document.URL.split("/")[2];
    var protocol = window.location.protocol;
    $("input[name='thx']").val(protocol+"//"+domain+Request.base+"/newsletter_thanks.html");
    $("input[name='err']").val(protocol+"//"+domain+Request.base+"/newsletter_error.php");

    $("#subscribe-form input[name='Page']").val(window.location.href);


    $('#subscribe-form').ajaxForm(options);



    var onFormResponse = function(response){
      response = JSON.parse(response);
      if (response.saved) {
        $("#connect-newsletter-submit-button").hide();
        $("#connect-newsletter-success-button").removeClass("hidden");
        $("#connect-subscribe-form input").val("");
      } else {
        alert(response.error);
      }
    };

    var beforeSubmit = function(){
      //$("#connect-subscribe-form input").attr("readonly", "readonly");
      return true;
    };

    var onError = function(err){
      alert("Error submitting newsletter. Please, try again.");

    }

    var options = {
        dataType: "jsonp",
        error: onError,
        iframe: true,
        beforeSubmit: beforeSubmit,
        success:       onFormResponse  // post-submit callback
    };

    /*$('#cannes-subscribe-form').ajaxForm(options);
    $("#cannes-subscribe-form input[name='Page']").val(window.location.href);

    var onFormResponse = function(response){
      response = JSON.parse(response);
      if (response.saved) {
        $("#cannes-subscribe-form-submit-button").hide();
        $("#cannes-subscribe-form-success-button").removeClass("hidden");
        $("#cannes-subscribe-form input").val("");
        $("#cannes-subscribe-form textarea").val("");
      } else {
        alert(response.error);
      }
    };

    var beforeSubmit = function(){
      //$("#connect-subscribe-form input").attr("readonly", "readonly");
      return true;
    };

    var onError = function(err){
      alert("Error submitting newsletter. Please, try again.");

    }

    var options = {
        dataType: "jsonp",
        error: onError,
        iframe: true,
        beforeSubmit: beforeSubmit,
        success:       onFormResponse  // post-submit callback
    };

    $('#cannes-subscribe-form').ajaxForm(options);
    $("#cannes-subscribe-form input[name='Page']").val(window.location.href);*/


}


Ozo.initSubscribeForm3 = function () {
  // bind form using 'ajaxForm'
    var domain = document.URL.split("/")[2];
    var protocol = window.location.protocol;

  $(".subscribe-form form").each(function(){
    var form = $(this);
    $("input[name='thx']",form).val(protocol+"//"+domain+Request.base+"/newsletter_thanks.html");
    $("input[name='err']",form).val(protocol+"//"+domain+Request.base+"/newsletter_error.php");
    $("input[name='Page']",form).val(window.location.href);

    var onFormResponse = function(response){
      response = JSON.parse(response);
      if (response.saved) {
          $(".submit-button",form).hide();
          $(".success-button",form).removeClass("hidden");
          $("input[type!='hidden']",form).val("");
      } else {
          alert(response.error);
      }
    };

    var beforeSubmit = function(){
      //$("#subscribe-form input").attr("readonly", "readonly");
      return true;
    };

    var onError = function(err){
      //$("#subscribe-form input").removeAttr("readonly");
      alert("Error submitting newsletter. Please, try again.");

    }

    var options = {
        dataType: "jsonp",
        error: onError,
        iframe: true,
        beforeSubmit: beforeSubmit,
        success:       onFormResponse  // post-submit callback
    };


      form.ajaxForm(options);


  });


}

Ozo.initSticky = function () {
    var s = "nav.page-menu-nav";
    var el = $(s);
    if (el.length < 1) {
        return;
    }
    var navOffset = jQuery(s).offset().top;
    jQuery(window).scroll(function () {
        var scrollPos = jQuery(window).scrollTop();
        if (scrollPos >= navOffset) {
            jQuery(s).addClass("sticky");
        } else {
            jQuery(s).removeClass("sticky");
        }
    });
}

Ozo.initBackTop = function () {
    var s = "#back-to-top";
    var el = $(s);
    if (el.length < 1) {
        return;
    }
    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('#back-to-top').tooltip('show');
}

Ozo.setCookie = function (name, value, exdays) {
    Cookies.set(name, value, {expires: exdays});

}

Ozo.getCookie = function (name) {
    return Cookies.get(name); // => 'value'
}

Ozo.allowSaveCookie = function () {
    Ozo.setCookie("show-alert-cookie-message", true, 10000);
    $("#cookie-alert-message").fadeOut();
}

Ozo.checkCookie = function (c_name) {
    var cookieName = Ozo.getCookie(c_name);
    if (cookieName != null && cookieName != "") {
        return false;
    }
    else {
        return true;
    }
}

Ozo.initCookies = function () {
    if (Ozo.checkCookie("show-alert-cookie-message")) {
        $("#cookie-alert-message").show();
    }
}

Ozo.owlCarouselTestimonials = function ()
{
    //Testimonials carousel
    var testimonailCarousel = $('.testimonial-list.owl-carousel');

    var initTestimonial = function ()
    {
        var firstTestimonial = testimonailCarousel.find('.owl-item:not(.cloned)').eq(0);
        jQuery(firstTestimonial).find('.testimonial').trigger('mouseover');
    };
    testimonailCarousel.on('initialized.owl.carousel', function (event) {
        initTestimonial();
    });
    //Initialize carousel;
    var reponsive = $('.testimonial-list').data('reponsive');

    testimonailCarousel.owlCarousel({
        nav: true,
        navText: [
            "<i class='fa fa-chevron-left'></i>",
            "<i class='fa fa-chevron-right'></i>"
        ],
        callbacks: true,
        loop: true,
        responsive: reponsive
    });
    //When translating adding trigger
    testimonailCarousel.on('translate.owl.carousel', function (event) {
        var item = event.item;
        testimonailCarousel.find('.owl-item').eq(item.index).find('.testimonial').trigger('mouseover');
    });
    testimonailCarousel.on('resize.owl.carousel', function (event) {
        initTestimonial();
        testimonailCarousel.trigger('to.owl.carousel', [0, 0, true]);
    });
};
Ozo.initTestimonials = function () {

    Ozo.owlCarouselTestimonials();

    //var reponsive = JSON.parse(dataReponsive);
    //console.log(reponsive);
    var testimonialList = $('.testimonial-list');
    testimonialList.find('.testimonial')
            .on('mouseover mouseup', function () {
                //Change the class becasuse owl carousel add class active
                $(this).closest('.testimonial-list').find('.testimonial').removeClass("show-testimonial");
                $(this).addClass("show-testimonial");
                var target = $(this).attr("data-target");
                $(target).siblings().removeClass("active");
                $(target).addClass("active");
            });
    if(testimonialList.hasClass('owl-carousel'))
    {

        $('.testimonial-list').find('.owl-item:not(.cloned)').eq(0).find('.testimonial').trigger('mouseover');

    }else{
        $('.testimonial-list').find('.testimonial').eq(0).trigger('mouseover');
    }

}


Ozo.smoothScroll = function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            var menuHeight = 0;
            if($(".page-menu-nav.sticky").length>0){
              menuHeight = $(".page-menu-nav.sticky").height();
            } else{
              if($(".page-menu-nav").length>0){
                menuHeight = $(".page-menu-nav").height() * 2;
              }
            }
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - menuHeight
                }, 1000);
                return false;
            }
        }
    });
}

Ozo.uniqId = function () {
    return Math.round(new Date().getTime() + (Math.random() * 100));
}

Ozo.panorama = function () {
    var container = $('div.panorama');
    if (container.length < 1 || container.hasClass('disabled')) {
        return;
    }
    var bg = container.attr('data-pano');
    if (!(bg.length > 0)) {
        return;
    }
    if (!container.attr("id")) {
        container.attr("id", Ozo.uniqId());
    }
    window.viewer = PhotoSphereViewer({
        panorama: bg,
        gyroscope: true,
        container: container.attr("id"),
        loading_img: Request.base + '/assets/img/ozo-logo.png',
        navbar: false,
        //default_fov: 100,
        mousewheel: false,
        size: {
            height: container.height()
        }
    });
    var lng = lat = false;
    lng = container.attr("data-position-lng");
    lat = container.attr("data-position-lat");
    if(Ozo.isMobile){
      lngMobile = container.attr("data-position-lng-mobile");
      latMobile = container.attr("data-position-lat-mobile");
      if(lngMobile&&latMobile){
        lng=lngMobile;
        lat=latMobile;
      }
    }
    if (lng && lat) {
        viewer.rotate({
            longitude: lng,
            latitude: lat
        });
    }
    viewer.on('ready', function () {
        container.children(".psv-container").css({"z-index": 0});
        viewer.stopAutorotate();
        setTimeout(function () {
            if (Ozo.isMobile) {
                viewer.startGyroscopeControl();
            }
        }, 1000);
    });
    /*var pano = container.pano({
     img: bg,
     interval: 100,
     speed: 50
     });

     pano.moveRight(100,5);*/

}

Ozo.initDropDown = function () {
    $('ul.nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
}

Ozo.initCarousel = function () {
    $('.carousel').carousel({
        interval: 0
    })
}

Ozo.initEqualHeightColumnsReset = function() {
  $(".equal-height-item").css("height","");
  Ozo.initEqualHeightColumns();

}
Ozo.initEqualHeightColumns = function ()
{
    if($(document).width()<902){
      return;
    }
    $(document).ready(function () {

        // Select and loop the container element of the elements you want to equalise
        $('.equal-height-container').each(function () {

            // Cache the highest
            var highestBox = 0;
            // Select and loop the elements you want to equalise
            $('.equal-height-item', this).each(function () {

                // If this box is higher than the cached highest then store it
                if ($(this).height() > highestBox) {
                    highestBox = $(this).height();
                }

            });
            // Set the height of all those children to whichever was highest
            $('.equal-height-item', this).height(highestBox);
        });
    });
}


Ozo.isTouchDevice = function(){
    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}
Ozo.initPophover = function(){
  if(Ozo.isTouchDevice()){
    return;
  }
  $('[data-toggle="popover"]').popover({
      html : true,
      trigger: 'manual',
      content: function() {
          return $(this).find(".pophover-content").html();
        }
    }
  ).on("mouseenter", function () {
    var _this = this;
    var isVisible = $(this).next('div.popover:visible').length;
    if(!isVisible)
      $(this).popover("show");
    $(".popover").on("mouseleave", function () {
        if(!$(_this).is(":hover"))
          $(_this).popover('hide');
    });
}).on("mouseleave", function () {
    var _this = this;
    setTimeout(function () {
        if (!$(".popover:hover").length && !$(_this).is(":hover")) {
            $(_this).popover("hide");
        }
    }, 300);
});
}

Ozo.initGoogleClickID = function(){
  function setCookie(name, value, days){
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    var expires = "; expires=" + date.toGMTString();
    document.cookie = name + "=" + value + expires+"; path=/";
    }
    function getParam(p){
        var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
    var gclid = getParam('gclid');
    if(gclid){
        var gclsrc = getParam('gclsrc');
        if(!gclsrc || gclsrc.indexOf('aw') !== -1){
            setCookie('gclid', gclid, 90);
        }
    }

    var gclidCookie = Ozo.getCookie("gclid");
    $("input[name='GCLID']").val(gclidCookie);
    $(".input-gclid").val(gclidCookie);
}

Ozo.initGoogleAnalyticsCookieParse = function(){


    /*
    var c_name = "__utmz";
    if (document.cookie.length>0){
        c_start=document.cookie.indexOf(c_name + "=");
        if (c_start!=-1){
            c_start=c_start + c_name.length+1;
            c_end=document.cookie.indexOf(";",c_start);
            if (c_end==-1) c_end=document.cookie.length;
            gc = unescape(document.cookie.substring(c_start,c_end));
        }
    }
    if(gc != ""){
        var z = gc.split('.');
        if(z.length >= 4){
        var y = z[4].split('|');
            for(i=0; i<y.length; i++){
                if(y[i].indexOf('utmcsr=') >= 0) ga_source = y[i].substring(y[i].indexOf('=')+1);
                if(y[i].indexOf('utmccn=') >= 0) ga_campaign = y[i].substring(y[i].indexOf('=')+1);
                if(y[i].indexOf('utmcmd=') >= 0) ga_medium = y[i].substring(y[i].indexOf('=')+1);
                if(y[i].indexOf('utmctr=') >= 0) ga_term = y[i].substring(y[i].indexOf('=')+1);
                if(y[i].indexOf('utmcct=') >= 0) ga_content = y[i].substring(y[i].indexOf('=')+1);
            }
        }
    }
*/

    function getParam(p){
        var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
    var ga_source = '';
    var ga_campaign = '';
    var ga_medium = '';
    var ga_term = '';
    var ga_content = '';
    var gc = '';

    ga_source = getParam('utm_source');
    ga_medium = getParam('utm_medium');
    ga_campaign = getParam('utm_campaign');

    if(ga_source){
      Ozo.setCookie('utm_source', ga_source,90);
    }
    if(ga_medium){
      Ozo.setCookie('utm_medium', ga_medium,90);
    }
    if(ga_campaign){
      Ozo.setCookie('utm_campaign', ga_campaign,90);
    }

    ga_source = Ozo.getCookie('utm_source');
    ga_medium = Ozo.getCookie('utm_medium');
    ga_campaign = Ozo.getCookie('utm_campaign');

    $(".input-ga-campaign").val(ga_campaign);
    $(".input-ga-source").val(ga_source);
    $(".input-ga-medium").val(ga_medium);
}

Ozo.initCarouselHero = function(){
  $('.owl-carousel-hero').owlCarousel({
      items:1,
      loop:true,
      autoplay:true
  });
}
Ozo.init = function () {
    $("[data-fancybox]").fancybox({
    });
    Ozo.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    if(!Ozo.isMobile){
      $("[data-fancybox-desktop]").fancybox({
      });
    }
    Ozo.initCookies();
    Ozo.initSticky();
    Ozo.initBackTop();
    Ozo.panorama();
    Ozo.initDropDown();
    Ozo.initCarousel();
    Ozo.initTestimonials();
    Ozo.initCarouselHero();
    Ozo.initEqualHeightColumns();
    Ozo.smoothScroll();
    Ozo.initSubscribeForm3();
    Ozo.initCannesContactForm();
    Ozo.initGoogleAnalyticsCookieParse();
    Ozo.initPophover();
    Ozo.initGoogleClickID();
    Ozo.initHorizontalCheckbox();
    $(window).on('resize', function () {
        Ozo.initEqualHeightColumns();
    });
}

$(function () {
    Ozo.init();
    new WOW().init();
})
