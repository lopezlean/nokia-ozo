OzoCamera = {};

OzoCamera.onPauseShowElement;
OzoCamera.playVideoTimeout;
OzoCamera.intervalRewind;
OzoCamera.headerSlide = true;
OzoCamera.onPause = function(){
  if(OzoCamera.onPauseShowElement){
    $(".video-toggle-item.active").removeClass("active");
    OzoCamera.onPauseShowElement.addClass("active").show();
    OzoCamera.onPauseShowElement = null;
  }
}
OzoCamera.playVideoReverse = function(stopAt){
       OzoCamera.intervalRewind = setInterval(function(){
           if(OzoCamera.video.currentTime == 0){
               clearInterval(OzoCamera.intervalRewind);
               OzoCamera.video.pause();
           }
           else{
               OzoCamera.video.currentTime += -.1;
           }
         },60);
    OzoCamera.playVideoTimeout = setTimeout(function(){
      clearInterval(OzoCamera.intervalRewind);
      OzoCamera.video.pause();
      OzoCamera.onPause();
      isPlaying  = false;
    },stopAt*1000);
}

OzoCamera.playVideo = function(stopAt){
  OzoCamera.video.play();
  OzoCamera.playVideoTimeout = setTimeout(function(){
      OzoCamera.video.pause();
      OzoCamera.onPause();
      isPlaying  = false;
  },stopAt*1000);
}

OzoCamera.play = function(toActive,playOnClick){
    if(toActive.length<1){
      toActive = $("#video-seek li:first-child");
    }
    toActive.addClass("active");
    currentIndex = parseInt(toActive.attr("data-seek-index"));
    currentSeek = parseFloat(toActive.attr("data-seek-time"));

    if(currentIndex >0){
      //$("header").css("display","none");
      //$("header").animate({ height: 0, opacity: 0 }, 'slow');
      $("header").slideUp(OzoCamera.resize);
      //OzoCamera.resize();

    } else{
      if(OzoCamera.headerSlide){
        $("header").slideDown(function(){
          OzoCamera.resize();
        });
      }
    }

    currentDurationTime = parseFloat(toActive.attr("data-seek-duration"));
    var rewind = currentSeek < OzoCamera.video.currentTime;
    if(parseInt(OzoCamera.video.currentTime)==currentSeek || (!playOnClick && currentSeek >= parseInt(OzoCamera.video.currentTime))){
      currentSeek = OzoCamera.video.currentTime;
    }
    OzoCamera.video.currentTime = currentSeek;
    isPlaying = true;


    var hasActive = $(".video-toggle-item.active").length > 0;
    if(hasActive){
        var fadeOutTime = playOnClick ? 0 : 800;
        $(".video-toggle-item.active").animate({opacity:0,bottom:"+=" + 20},fadeOutTime,function(){
          $(this).removeClass("active");
          $(this).removeAttr("style");

            OzoCamera.playVideo(currentDurationTime);

            if(playOnClick){
              $(".video-toggle-item.active").removeClass("active").hide();
              OzoCamera.onPauseShowElement = $(".video-toggle-item.data-seek-index-"+currentIndex);
            } else{
              OzoCamera.onPauseShowElement = $(".video-toggle-item.data-seek-index-"+currentIndex);
            }

            if(currentIndex==10){
              setTimeout(function(){
                  OzoCamera.endAnimation();
              },1900);
            }
        });

    } else{
      $(".video-toggle-item.data-seek-index-"+currentIndex).addClass("active").show();
      OzoCamera.playVideo(currentDurationTime);
    }


}

var currentSeek = 0;
var currentDurationTime = 0;
var isPlaying = false;

OzoCamera.loading = true;

OzoCamera.resize = function(){
  //video res: 1120x770
  var useWidth = $(window).height() > $(window).width();




  //var headerHeight = $("header").is(":visible") ? $("header").height() : 0;
  var headerHeight = 0;
  var optimalHeight = $(window).height() - headerHeight - $(".page-menu-nav").height();

  $("#v0").height(optimalHeight);
  $(".video.panorama-internal").height(optimalHeight);

  var olTop = (optimalHeight / 2) - ($("#video-section ol.list").height() / 2);
  $("#video-section ol.list").css({top:olTop});

  if(useWidth){
    $("#v0").width("100%");
    $("#v0").height("100%");
  } else{
    $("#v0").width("auto");
    if($("#v0").width()>$(window).width()){
      $("#v0").width("100%");
      $("#v0").height("100%");
    }
  }
}
OzoCamera.animationEnded = false;

OzoCamera.animationStart = function(){
  $("#after-video").hide();
  $("footer").hide();
  OzoCamera.lockScroll();
  OzoCamera.keyboardStart();
  //$("#video-section").css({"margin-bottom":1000});
  $("#video-section .video-container").css({"position":"fixed"});
  OzoCamera.animationEnded = false;
}

OzoCamera.endAnimation = function(){
  if(OzoCamera.animationEnded){
    return;
  }
  OzoCamera.animationEnded = true;
  OzoCamera.unlockScroll();
  OzoCamera.keyboardEnd()
  $("#after-video").show();
  Ozo.initEqualHeightColumnsReset();
  $("footer").show();
  $(".video-toggle-item").removeClass("active");
  //$("#video-section").css({"margin-bottom":0});
  $("#video-section .video-container").css({"position":"absolute"});
  var goTo = $("#after-video").offset().top;
  var menuHeight = $(".page-menu-nav").height() * 2;
  $('html, body').animate({
      scrollTop: goTo - menuHeight,
  }, 2000,null,function(){
    $(".video-toggle-item-show-more").remove();
    $("#video-seek li").removeClass("active");
    OzoCamera.headerSlide = false;
    OzoCamera.play("");
    OzoCamera.headerSlide = true;
  });


  /*$("#video-section").slideUp(1000,function(){
    //$("header").slideDown();
  });*/
}



OzoCamera.keyboardHandler = function(e){
  var UP = 38;
  var DOWN = 40;
  var SPACE = 32;

  var code = e.keyCode;
  if(code==DOWN || code==SPACE){
    OzoCamera.scrollVideo(true);
  }
  if(code==UP){
    OzoCamera.scrollVideo(false);
  }

}

OzoCamera.keyboardStart = function(){
  $(window).keyup(OzoCamera.keyboardHandler);

}

OzoCamera.keyboardEnd = function(){
  $(window).unbind('keyup',OzoCamera.keyboardHandler);
}

OzoCamera.scrollVideo = function(next){
  // white for animation stop
  if(isPlaying){
    return;
  }
  var toActive;
  if(next){
    toActive = $("#video-seek li.active").next();
  } else{
    toActive = $("#video-seek li.active").prev();
  }


  $("#video-seek li").removeClass("active");
  if(toActive.length<1&&!next){
    return;
  }
  OzoCamera.play(toActive,false);

}

OzoCamera.video = null;
OzoCamera.init = function(){
    $("#scroll-to-continue-info").click(function(e){
      e.preventDefault();
      OzoCamera.scrollVideo(true);
      return false;
    });
    $(".video-loading").hide();
    $("#video-section ol.list").fadeIn();
    OzoCamera.video = document.getElementById('v0');
    OzoCamera.video.load();
    $(".page-menu-nav li a").click(function(){
      OzoCamera.unlockScroll();
      return true;
    })

    OzoCamera.resize();

    $(window).on('resize orientationChange', function(event) {
      OzoCamera.resize();
    });


    // dynamically set the page height according to video length
    OzoCamera.video.addEventListener('loadeddata', function(e) {
      OzoCamera.loading = false;
    });



    OzoCamera.video.addEventListener('timeupdate', function() {
      if(this.currentTime >= (currentSeek + currentDurationTime)){
        this.pause();
        OzoCamera.onPause();
        isPlaying  = false;
      }
    });

    OzoCamera.video.addEventListener('ended',function(){
      OzoCamera.endAnimation();
    },false);


    // next: true go to next frame, false go to previus

    $("#video-seek li").click(function(){
      if(OzoCamera.loading || !OzoCamera.video.paused){
        return false;
      }
      if(OzoCamera.animationEnded){
        window.scrollTo(0, 0);
        OzoCamera.animationStart();
      }
      clearTimeout(OzoCamera.playVideoTimeout);
      $("#video-seek li").removeClass("active");
      var toActive = $(this);
      OzoCamera.play(toActive,true);
    });

    OzoCamera.play("",false);

    OzoCamera.animationStart();
}

OzoCamera.scrollLocked = true;
OzoCamera.scrollListen = function(){
  //Firefox
  $('html').bind('DOMMouseScroll', function(e){
      if(!OzoCamera.scrollLocked){
        return true;
      }

      if(OzoCamera.loading){
        return false;
      }

      if(e.originalEvent.detail > 0) {
          //scroll down
          OzoCamera.scrollVideo(true);
      }else {
          //scroll up
          OzoCamera.scrollVideo(false);
      }

      //prevent page fom scrolling
      return !OzoCamera.scrollLocked;
  });
  $('html').bind('mousewheel',function(e) {
      if(!OzoCamera.scrollLocked){
        return true;
      }
      if(OzoCamera.loading){
        return false;
      }
      if(e.originalEvent.wheelDelta < 0) {
          OzoCamera.scrollVideo(true);
      }else {
          //scroll up
          OzoCamera.scrollVideo(false);
      }
    //prevent page fom scrolling
      return !OzoCamera.scrollLocked;
  });
}
OzoCamera.lockScroll = function(){
  var scrollPosition = [
  self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
  self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
  ];
  var html = jQuery('html'); // it would make more sense to apply this to body, but IE7 won't have that
  html.data('scroll-position', scrollPosition);
  html.data('previous-overflow', html.css('overflow'));
  html.css('overflow', 'hidden');
  window.scrollTo(scrollPosition[0], scrollPosition[1]);
  OzoCamera.scrollLocked = true;

}

OzoCamera.unlockScroll = function(){
  var html = jQuery('html');
  var scrollPosition = html.data('scroll-position');
  html.css('overflow', "auto");
  window.scrollTo(scrollPosition[0], scrollPosition[1])
  OzoCamera.scrollLocked = false;
}

OzoCamera.loadVideo = function(callback){
  var xhr = new XMLHttpRequest();
  xhr.open('GET', Request.base+"/assets/videos/ozo-camera.mp4", true);
  xhr.responseType = 'blob';
  xhr.onload = function(e) {
    if (this.status == 200) {
      var data = this.response;
      var videoObject = URL.createObjectURL(data);
      var video = document.getElementById("v0");
      video.src = videoObject;
      callback();

     }
    }

    function updateProgress (oEvent) {
      if (oEvent.lengthComputable) {
        var percentComplete = parseInt((oEvent.loaded / oEvent.total) * 100);
        $(".video-loading p").html(percentComplete+ " %");
        // ...
      } else {
        // Unable to compute progress information since the total size is unknown
      }
    }

  xhr.addEventListener("progress", updateProgress, false);


  xhr.send();

};
$(function(){
  $('[data-toggle="tooltip"]').tooltip();
  if(!Ozo.isMobile){
    OzoCamera.resize();
    $(".video-loading").show();
    OzoCamera.lockScroll();
    OzoCamera.scrollListen();
    OzoCamera.loadVideo(OzoCamera.init);
    window.onbeforeunload = function () {
      window.scrollTo(0, 0);
    }
  } else{
    $("body").addClass("mobile");
    $("#mobile-content").removeClass("hidden");
  }

})
