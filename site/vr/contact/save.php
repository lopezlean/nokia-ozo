<?php
require_once("config.php");
$requiredFields = ['first_name','email','company'];
foreach($requiredFields as $f){
    if(empty($_REQUEST[$f])){
      $response = array(
        'saved'=>false
      );

      // ,'phone','00N5800000BsGoX'

      echo json_encode($response);
      return;
    }
  }
    $name = $_REQUEST['first_name'];
    $email = $_REQUEST['email'];
    $phone = $_REQUEST['phone'];
    $comments = $_REQUEST['00N5800000BsGoX'];
    $company = $_REQUEST['company'];



    try{
        require("config.php");
        // Create (connect to) SQLite database in file
        $db = new PDO("mysql:host=$host;dbname=$dbname",$username,$password);

        //$db = new PDO('sqlite:emails.sqlite3');

            $db->exec("
            CREATE TABLE IF NOT EXISTS `cannes` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(255) DEFAULT NULL,
              `email` varchar(255) DEFAULT NULL,
              `company` varchar(255) DEFAULT NULL,
              `phone` varchar(255) DEFAULT NULL,
              `comments` text,
                PRIMARY KEY (id)
            ) ENGINE=InnoDB ;");

        // Set errormode to exceptions
        $db->setAttribute(PDO::ATTR_ERRMODE,
                                PDO::ERRMODE_EXCEPTION);


      $insert = "INSERT INTO cannes (name, email,company, phone,comments)
                  VALUES (:name,
                      :email,
                      :company,
                      :phone,
                      :comments)";

      $stmt = $db->prepare($insert);

      // Bind parameters to statement variables
      $stmt->bindParam(':name', $name);
      $stmt->bindParam(':email', $email);
      $stmt->bindParam(':company', $company);
      $stmt->bindParam(':phone', $phone);
      $stmt->bindParam(':comments', $comments);

//      $stmt->bindParam(':created', date("Y-m-d h:i:s"));


    $stmt->execute();
    }
    catch(PDOException $e) {
      // Print PDOException message
      echo $e->getMessage();
      $response = array(
        'saved'=>false
      );

      echo json_encode($response);
      return;

    }
    $db = null;



    $response = array(
      'saved'=>true
    );



echo json_encode($response);
