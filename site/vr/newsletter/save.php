<?php

if(empty($_REQUEST['email'])){
  $response = array(
    'saved'=>false
  );

  echo json_encode(compact('response'));
  return;
}

    date_default_timezone_set('UTC');

    /*$fp = fopen('emails.csv', 'a');

    $email = $_REQUEST['email'];
    $url = $_SERVER["HTTP_REFERER"];
    $created = date("Y-m-d h:i:s");
    $toSave = array($email,$url,$created);
    fputcsv($fp, $toSave);
    fclose($fp);*/


try{
    require("config.php");
    // Create (connect to) SQLite database in file
    $db = new PDO("mysql:host=$host;dbname=$dbname",$username,$password);

    //$db = new PDO('sqlite:emails.sqlite3');

    /*$db->exec("CREATE TABLE IF NOT EXISTS emails (
                   id INTEGER PRIMARY KEY,
                   email TEXT,
                   referer TEXT,
                  created DATETIME)");*/

    // Set errormode to exceptions
    $db->setAttribute(PDO::ATTR_ERRMODE,
                            PDO::ERRMODE_EXCEPTION);

  $email = $_REQUEST['email'];
  $url = $_SERVER["HTTP_REFERER"];


  $insert = "INSERT INTO emails (email, referer, created)
              VALUES (:email,
                  :referer,
                  :created)";

  $stmt = $db->prepare($insert);

  // Bind parameters to statement variables
  $stmt->bindParam(':email', $_REQUEST['email']);
  $stmt->bindParam(':referer', $url);
  $stmt->bindParam(':created', date("Y-m-d h:i:s"));


$stmt->execute();
}
catch(PDOException $e) {
  // Print PDOException message
  echo $e->getMessage();
  $response = array(
    'saved'=>false
  );

  echo json_encode(compact('response'));
  return;

}
$db = null;



$response = array(
  'saved'=>true
);

echo json_encode(compact('response'));
