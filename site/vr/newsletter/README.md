DATABASE CONFIGURATION
----------------------

Copy config.php.default to config.php and set real values


if the database doesnt yet exist, in bash console execute:
---

mysql -u username -h databasehost -p

will login into mysql console, inside this console create the database
---

create database ozo;
exit


 Load database tables with db.sql file (This file is inside the source code)
 ---

mysql -u username -h databasehost -p < db.sql
